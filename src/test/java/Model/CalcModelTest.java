package Model;

import Model.Exceptions.DifferentVariablePolynomialsException;
import Model.OperationsAdapter.PolynomialOperation;
import Model.Parser.EmptyPolynomialStringException;
import Model.Parser.IllegalPolynomialCharacterException;
import Model.Parser.IllegalPolynomialStringPartException;
import Model.Parser.MultipleVariablePolynomialException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalcModelTest {

    private static final String polyA = "4x^4 + 3 + 1*x^3";
    private static final String polyB = "10*x^3+5x^2+3";
    private static final String polyC = "100y^5+100y^2";
    private static final String polyD = "100y^(5)+100y^2";
    private static final String polyE = "100y^5+100x^2";
    private static final String polyF = "100y^5+^y100y^2";
    private static final String polyG = "y^10-5y";
    private static final String polyH = "-5y^10";
    private static final String polyI = "2";
    private static final String polyJ = "a";
    private CalcModel calcModel;

    @Test
    void compute_ADD() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        String a_add_b = "4x^4+11x^3+5x^2+6";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.ADD);
        calcModel.compute(polyA, polyB);
        assertEquals(a_add_b, calcModel.getResult1String());
        assertEquals("", calcModel.getResult2String());
    }

    @Test
    void compute_SUBTRACT() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        String a_min_b = "4x^4-9x^3-5x^2";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.SUBTRACT);
        calcModel.compute(polyA, polyB);
        assertEquals(a_min_b, calcModel.getResult1String());
        assertEquals("", calcModel.getResult2String());
    }

    @Test
    void compute_MULTIPLY() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        String a_times_b = "40x^7+30x^6+5x^5+12x^4+33x^3+15x^2+9";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.MULTIPLY);
        calcModel.compute(polyA, polyB);
        assertEquals(a_times_b, calcModel.getResult1String());
        assertEquals("", calcModel.getResult2String());
    }

    @Test
    void compute_DIVIDE() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        String quot = "0.4x-0.1";
        String rem = "0.5x^2-1.2x+3.3";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.DIVIDE);
        calcModel.compute(polyA, polyB);
        assertEquals(quot, calcModel.getResult1String());
        assertEquals(rem, calcModel.getResult2String());
    }

    @Test
    void compute_DIFFERENTIATE() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        String deriv = "16x^3+3x^2";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.DIFFERENTIATE);
        calcModel.compute(polyA, polyB);
        assertEquals(deriv, calcModel.getResult1String());
        assertEquals("", calcModel.getResult2String());
    }

    @Test
    void compute_INTEGRATE() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        String antideriv = "0.8x^5+0.25x^4+3x";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.INTEGRATE);
        calcModel.compute(polyA, polyB);
        assertEquals(antideriv, calcModel.getResult1String());
        assertEquals("", calcModel.getResult2String());
    }

    @Test
    void compute_ADD_nonDefaultVariableCharacter() throws MultipleVariablePolynomialException,
            DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        String g_add_h = "-4y^10-5y";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.ADD);
        calcModel.compute(polyG, polyH);
        assertEquals(g_add_h, calcModel.getResult1String());
        assertEquals("", calcModel.getResult2String());
    }

    @Test
    void compute_ADD_missingVariableCharacter_nonDefaultVariable() throws MultipleVariablePolynomialException,
            DifferentVariablePolynomialsException, IllegalPolynomialCharacterException, IllegalPolynomialStringPartException, EmptyPolynomialStringException {
        // added based on a failure which was discovered at a late phase of the development
        String i_add_j = "a+2";
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.ADD);
        calcModel.compute(polyI, polyJ);
        assertEquals(i_add_j, calcModel.getResult1String());
        assertEquals("", calcModel.getResult2String());
    }

    @Test
    void getOperation() {
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.INTEGRATE);
        assertEquals(PolynomialOperation.INTEGRATE, calcModel.getOperation());
    }

    @Test
    void compute_expectDifferentVariablePolynomialsException() {
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.ADD);
        Assertions.assertThrows(DifferentVariablePolynomialsException.class, () -> {
            calcModel.compute(polyA, polyC);
        });
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult1String());
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult2String());
    }

    @Test
    void compute_expectMultipleVariablePolynomialException() {
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.ADD);
        Assertions.assertThrows(MultipleVariablePolynomialException.class, () -> {
            calcModel.compute(polyE, polyA);
        });
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult1String());
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult2String());
    }

    @Test
    void compute_expectIllegalPolynomialCharacterException() {
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.ADD);
        Assertions.assertThrows(IllegalPolynomialCharacterException.class, () -> {
            calcModel.compute(polyD, polyA);
        });
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult1String());
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult2String());
    }

    @Test
    void compute_expectIllegalPolynomialStringPartException() {
        calcModel = new CalcModel();
        calcModel.setOperation(PolynomialOperation.ADD);
        Assertions.assertThrows(IllegalPolynomialStringPartException.class, () -> {
            calcModel.compute(polyF, polyD);
        });
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult1String());
        assertEquals(CalcModel.INVALID_RESULT, calcModel.getResult2String());
    }
}