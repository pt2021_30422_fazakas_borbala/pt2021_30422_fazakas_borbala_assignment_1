package Model.Parser;

import Model.Data.Monomial;
import Model.Data.Polynomial;
import Model.Exceptions.DifferentVariablePolynomialsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PolynomialParserTest {

    private static PolynomialParser parser;

    /**
     * ---------------------------------valid polynomials----------------------------------------
     */
    private static final String vs1 = "x^3+75x+198"; //positive coefficients, no multiplication sign
    private static final Polynomial vp1 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(1, 3))
            .addMonomial(Monomial.constructMonomial(75, 1))
            .addMonomial(Monomial.constructMonomial(198, 0))
            .build();
    private static final String vs2 = "-x^106-356x-89"; //positive coefficients, no multiplication
    // sign
    private static final Polynomial vp2 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(-1, 106))
            .addMonomial(Monomial.constructMonomial(-356, 1))
            .addMonomial(Monomial.constructMonomial(-89, 0))
            .build();
    private static final String vs3 = "-308x^45+858x^5+78x^2-3002x"; //mixed coefficients,
    // multiplication sign present
    private static final Polynomial vp3 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(-308, 45))
            .addMonomial(Monomial.constructMonomial(858, 5))
            .addMonomial(Monomial.constructMonomial(78, 2))
            .addMonomial(Monomial.constructMonomial(-3002, 1))
            .build();
    private static final String vs4 = "x ^  13 + 705  x + 10"; //mixed coefficients, some
    // multiplication
    // signs, additional whitespaces
    private static final Polynomial vp4 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(1, 13))
            .addMonomial(Monomial.constructMonomial(705, 1))
            .addMonomial(Monomial.constructMonomial(10, 0))
            .build();
    private static final String vs5 = "-x ^  100"; // single term
    private static final Polynomial vp5 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(-1, 100))
            .build();
    private static final String vs6 = "-50"; // single term, 0 exponent
    private static final Polynomial vp6 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(-50, 0))
            .build();
    private static final String vs7 = "0"; // zero
    private static final Polynomial vp7 = new Polynomial.StandardBuilder()
            .build();
    private static final String vs8 = "-0"; //m negative zero
    private static final Polynomial vp8 = new Polynomial.StandardBuilder()
            .build();
    private static final String vs9 = "+x^3-x^4+39*x^100"; // randomized
    // monomial order, repeated exponents
    private static final Polynomial vp9 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(39, 100))
            .addMonomial(Monomial.constructMonomial(-1, 4))
            .addMonomial(Monomial.constructMonomial(1, 3))
            .build();
    private static final String vs10 = "-308x^45+858x^5+100*x^45+78x^2-79x^40-3002x"; // randomized
    // monomial order, repeated exponents
    private static final Polynomial vp10 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(-208, 45))
            .addMonomial(Monomial.constructMonomial(-79, 40))
            .addMonomial(Monomial.constructMonomial(858, 5))
            .addMonomial(Monomial.constructMonomial(78, 2))
            .addMonomial(Monomial.constructMonomial(-3002, 1))
            .build();
    private static final String vs11 = "+p^3-p^4+39*p^100"; // variable p
    private static final Polynomial vp11 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(39, 100))
            .addMonomial(Monomial.constructMonomial(-1, 4))
            .addMonomial(Monomial.constructMonomial(1, 3))
            .build();
    private static final String vs12 = "-308a^45+858a^5+100*a^45+78a^2-79a^40-3002a"; // variable a
    private static final Polynomial vp12 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(-208, 45))
            .addMonomial(Monomial.constructMonomial(-79, 40))
            .addMonomial(Monomial.constructMonomial(858, 5))
            .addMonomial(Monomial.constructMonomial(78, 2))
            .addMonomial(Monomial.constructMonomial(-3002, 1))
            .build();
    private static final String vs13 = "1 + t"; // based on failed manual testing
    private static final Polynomial vp13 = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(1, 1))
            .addMonomial(Monomial.constructMonomial(1, 0))
            .build();

    /**
     * -------------------------invalid polynomials: multiple variables--------------------------
     */
    private static final String imvs1 = "p^3-p^4+39*q^100";
    private static final Set<Character> variables1 = Set.of('p', 'q');
    private static final String imvs2 = "-308a^45+858a^5+100*c^45+78a^2-79b^40-3002a";
    private static final Set<Character> variables2 = Set.of('a', 'b', 'c');

    /**
     * -------------------------invalid polynomials: illegal characters--------------------------
     */
    private static final String ics1 = "p^3-(3+5)*7";
    private static final int icp1 = 4;
    private static final String ics2 = "3x^100+16&\\9-8+76";
    private static final int icp2 = 9;


    /**
     * ---------------------invalid polynomials: invalid part -----------------------------------
     */
    private static final String is1 = "78p^3p-p^4+39*p^100";
    private static final String isp1 = "p-p^4+39*p^100";
    private static final String is3 = "x^2^3-9";
    private static final String isp3 = "^3-9";
    private static final String is4 = "-100x-x^x";
    private static final String isp4 = "^x";
    private static final String isp5 = "";
    private static final String is6 = "x^3-56*x^2+-3";
    private static final String isp6 = "+-3";
    private static final String is7 = "-3^2+2x";
    private static final String isp7 = "^2+2x";
    private static final String is8 = "-32x^2+6 7x";
    private static final String isp8 = "7x";
    private static final String is9 = "+-x^2";
    private static final String isp9 = "+-x^2";
    private static final String is10 = "x+3x+4^x";
    private static final String isp10 = "^x";

    @BeforeAll
    static void setUp() {
        parser = new PolynomialParser();
    }

    @ParameterizedTest
    @MethodSource("provideValidInput")
    void parse_validPolynomial(String s, Polynomial expected) throws MultipleVariablePolynomialException, IllegalPolynomialStringPartException, IllegalPolynomialCharacterException, EmptyPolynomialStringException {
        assertEquals(expected, parser.parse(s));
    }

    @ParameterizedTest
    @MethodSource("provideInalidMultipleVariableInput")
    void parse_invalidPolynomial_expectMultipleVariablesException(String s,
                                                                  Set<Character> expectedVariables) {
        MultipleVariablePolynomialException e = Assertions.assertThrows(MultipleVariablePolynomialException.class, () -> {
            parser.parse(s);
        });
        assertEquals(expectedVariables, e.getVariables());
    }

    @ParameterizedTest
    @MethodSource("provideIllegalCharacterInput")
    void parse_invalidPolynomial_expectIllegalPolynomialCharacterEXception(String s,
                                                                           int illCharacPosition) {
        IllegalPolynomialCharacterException e = Assertions.assertThrows(IllegalPolynomialCharacterException.class, () -> {
            parser.parse(s);
        });
        assertEquals(illCharacPosition, e.getIllegalCharacterPosition());
    }

    @ParameterizedTest
    @MethodSource("provideInvalidInput")
    void parse_invalidPolynomial_expectInvalidPartException(String s, String invalidPart) {
        IllegalPolynomialStringPartException e = Assertions.assertThrows(IllegalPolynomialStringPartException.class, () -> {
            parser.parse(s);
        });
        assertEquals(invalidPart, e.getWrongSubstring());
    }

    @Test
    void parse_invalidPolynomial_expectEmptyPolynomialStringEXception() {
        Assertions.assertThrows(EmptyPolynomialStringException.class, () -> {
            parser.parse("");
        });
    }

    @Test
    void getOperandsSingleVariableCharacter_singleCharacter() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException {
        String s1 = "2u^3+3u";
        String s2 = "102 u+3u^5+2";
        Assertions.assertEquals('u', parser.getOperandsSingleVariableCharacter(s1, s2));
    }

    @Test
    void getOperandsSingleVariableCharacter_multipleCharacters() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException {
        String s1 = "2u^3+3u";
        String s2 = "102 u+3x^5+2";
        Assertions.assertThrows(MultipleVariablePolynomialException.class, () -> {
            parser.getOperandsSingleVariableCharacter(s1, s2);
        });
    }

    @Test
    void getOperandsSingleVariableCharacter_differentCharacters() throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException {
        String s1 = "2u^3+3u";
        String s2 = "102 x+3x^5+2";
        Assertions.assertThrows(DifferentVariablePolynomialsException.class, () -> {
            parser.getOperandsSingleVariableCharacter(s1, s2);
        });
    }

    @Test
    void getOperandsSingleVariableCharacter_oneStringHasNoCharacter() throws MultipleVariablePolynomialException,
            DifferentVariablePolynomialsException {
        String s1 = "2";
        String s2 = "102 t+3t^5+2";
        Assertions.assertEquals('t', parser.getOperandsSingleVariableCharacter(s1, s2));
    }

    @Test
    void getOperandsSingleVariableCharacternoVariableCharacter_expectDefault() throws MultipleVariablePolynomialException,
            DifferentVariablePolynomialsException {
        String s1 = "2";
        String s2 = "1024";
        Assertions.assertEquals(Monomial.DEFAULT_VARIABLE_CHARACTER, parser.getOperandsSingleVariableCharacter(s1, s2));
    }


    private static List<Arguments> provideValidInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(vs1, vp1),
                Arguments.of(vs2, vp2),
                Arguments.of(vs3, vp3),
                Arguments.of(vs4, vp4),
                Arguments.of(vs5, vp5),
                Arguments.of(vs6, vp6),
                Arguments.of(vs7, vp7),
                Arguments.of(vs8, vp8),
                Arguments.of(vs9, vp9),
                Arguments.of(vs10, vp10),
                Arguments.of(vs11, vp11),
                Arguments.of(vs12, vp12),
                Arguments.of(vs13, vp13)));
    }

    private static List<Arguments> provideInalidMultipleVariableInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(imvs1, variables1),
                Arguments.of(imvs2, variables2)));
    }

    private static List<Arguments> provideIllegalCharacterInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(ics1, icp1),
                Arguments.of(ics2, icp2)));
    }

    private static List<Arguments> provideInvalidInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(is1, isp1),
                Arguments.of(is3, isp3),
                Arguments.of(is4, isp4),
                Arguments.of(is6, isp6),
                Arguments.of(is7, isp7),
                Arguments.of(is8, isp8),
                Arguments.of(is9, isp9),
                Arguments.of(is10, isp10)));
    }

}