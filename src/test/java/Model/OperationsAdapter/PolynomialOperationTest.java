package Model.OperationsAdapter;

import Model.Data.Monomial;
import Model.Data.Polynomial;
import javafx.util.Pair;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PolynomialOperationTest {

    private static final Polynomial a = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(4, 4))
            .addMonomial(Monomial.constructMonomial(1, 3))
            .addMonomial(Monomial.constructMonomial(3, 0))
            .build();
    private static final Polynomial b = new Polynomial.StandardBuilder()
            .addMonomial(Monomial.constructMonomial(10, 3))
            .addMonomial(Monomial.constructMonomial(5, 2))
            .addMonomial(Monomial.constructMonomial(3, 0))
            .build();

    @Test
    void compute_ADD() {
        Polynomial a_add_b = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(4, 4))
                .addMonomial(Monomial.constructMonomial(11, 3))
                .addMonomial(Monomial.constructMonomial(5, 2))
                .addMonomial(Monomial.constructMonomial(6, 0))
                .build();
        Pair<Polynomial, Optional<Polynomial>> expected = new Pair<>(a_add_b, Optional.empty());
        assertEquals(expected, PolynomialOperation.ADD.compute(a, b));
    }

    @Test
    void compute_SUBTRACT() {
        Polynomial a_min_b = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(4, 4))
                .addMonomial(Monomial.constructMonomial(-9, 3))
                .addMonomial(Monomial.constructMonomial(-5, 2))
                .build();
        Pair<Polynomial, Optional<Polynomial>> expected = new Pair<>(a_min_b, Optional.empty());
        assertEquals(expected, PolynomialOperation.SUBTRACT.compute(a, b));
    }

    @Test
    void compute_MULTIPLY() {
        Polynomial a_times_b = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(40, 7))
                .addMonomial(Monomial.constructMonomial(30, 6))
                .addMonomial(Monomial.constructMonomial(5, 5))
                .addMonomial(Monomial.constructMonomial(12, 4))
                .addMonomial(Monomial.constructMonomial(33, 3))
                .addMonomial(Monomial.constructMonomial(15, 2))
                .addMonomial(Monomial.constructMonomial(9, 0))
                .build();
        Pair<Polynomial, Optional<Polynomial>> expected = new Pair<>(a_times_b, Optional.empty());
        assertEquals(expected, PolynomialOperation.MULTIPLY.compute(a, b));
    }

    @Test
    void compute_DIVIDE() {
        Polynomial quot = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(0.4, 1))
                .addMonomial(Monomial.constructMonomial(-0.1, 0))
                .build();
        Polynomial rem = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(0.5, 2))
                .addMonomial(Monomial.constructMonomial(-1.2, 1))
                .addMonomial(Monomial.constructMonomial(3.3, 0))
                .build();
        Pair<Polynomial, Optional<Polynomial>> expected = new Pair<>(quot, Optional.of(rem));
        assertEquals(expected, PolynomialOperation.DIVIDE.compute(a, b));
    }

    @Test
    void compute_DIFFERENTIATE() {
        Polynomial deriv = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(16, 3))
                .addMonomial(Monomial.constructMonomial(3, 2))
                .build();
        Pair<Polynomial, Optional<Polynomial>> expected = new Pair<>(deriv, Optional.empty());
        assertEquals(expected, PolynomialOperation.DIFFERENTIATE.compute(a, b));
    }

    @Test
    void compute_INTEGRATE() {
        Polynomial deriv = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(0.8, 5))
                .addMonomial(Monomial.constructMonomial(0.25, 4))
                .addMonomial(Monomial.constructMonomial(3, 1))
                .build();
        Pair<Polynomial, Optional<Polynomial>> expected = new Pair<>(deriv, Optional.empty());
        assertEquals(expected, PolynomialOperation.INTEGRATE.compute(a, b));
    }
}