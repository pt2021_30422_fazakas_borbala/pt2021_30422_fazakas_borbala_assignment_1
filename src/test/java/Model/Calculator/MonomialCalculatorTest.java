package Model.Calculator;

import Model.Data.Monomial;
import javafx.util.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class MonomialCalculatorTest {

    private static MonomialCalculator monomialCalculator;

    private static final Monomial a = Monomial.constructMonomial(3, 3);
    private static final Monomial b = Monomial.constructMonomial(5, 3);
    private static final Monomial c = Monomial.constructMonomial(-3, 3);
    private static final Monomial d = Monomial.constructMonomial(2, 2);
    private static final Monomial e = Monomial.constructMonomial(0, 3);
    private static final Monomial f = Monomial.constructMonomial(3, 0);
    private static final Monomial h = Monomial.constructMonomial(2.5, 3);
    private static final Monomial i = Monomial.constructMonomial(-2.3, 3);
    private static final Monomial j = Monomial.constructMonomial(-5.8, 3);
    private static final Monomial k = Monomial.constructMonomial(11.3, 2);
    private static final Monomial l = Monomial.constructMonomial(12, 5);
    private static final Monomial n = Monomial.constructMonomial(12, 0);
    private static final Monomial ONE = Monomial.constructMonomial(1, 0);

    @BeforeAll
    static void setUp() {
        monomialCalculator = MonomialCalculator.getInstance();
    }

    @ParameterizedTest
    @MethodSource("provideAddInput")
    void add(Monomial m1, Monomial m2, Monomial expected) {
        Monomial actual = monomialCalculator.add(m1, m2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed monomial addition: %s + %s", m1, m2));
    }

    @Test
    void add_unequalExponents_expectException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            monomialCalculator.add(a, d);
        });
    }

    @ParameterizedTest
    @MethodSource("provideSubtractInput")
    void subtract(Monomial m1, Monomial m2, Monomial expected) {
        Monomial actual = monomialCalculator.subtract(m1, m2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed monomial subtraction: %s - %s", m1, m2));
    }

    @Test
    void subtract_unequalExponents_expectException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            monomialCalculator.subtract(a, d);
        });
    }

    @ParameterizedTest
    @MethodSource("provideMultiplyInput")
    void multiply(Monomial m1, Monomial m2, Monomial expected) {
        Monomial actual = monomialCalculator.multiply(m1, m2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed monomial multiplication: %s * %s", m1, m2));
    }

    @ParameterizedTest
    @MethodSource("provideDivideInput")
    void divide(Monomial m1, Monomial m2, Pair<Monomial, Monomial> expected) {
        Pair<Monomial, Monomial> actual = monomialCalculator.divide(m1, m2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed monomial multiplication: %s / %s", m1, m2));
    }

    @Test
    void divide_zeroDivisor_expectException() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            monomialCalculator.divide(a, e);
        });
    }

    @ParameterizedTest
    @MethodSource("provideDifferentiateInput")
    void differentiate(Monomial m, Monomial expected) {
        Monomial actual = monomialCalculator.differentiate(m);
        Assertions.assertEquals(expected, actual,
                String.format("Failed monomial differentiation: (%s)'", m));
    }

    @ParameterizedTest
    @MethodSource("provideIntegrateInput")
    void integrate(Monomial m, Monomial expected) {
        Monomial actual = monomialCalculator.integrate(m);
        Assertions.assertEquals(expected, actual,
                String.format("Failed monomial integration: Integral(%s)", m));
    }

    @ParameterizedTest
    @MethodSource("provideOppositeInput")
    void opposite(Monomial m, Monomial expected) {
        Monomial actual = monomialCalculator.opposite(m);
        Assertions.assertEquals(expected, actual,
                String.format("Failed monomial opposite computation: Opposite(%s)", m));
    }

    private static List<Arguments> provideAddInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, c, Monomial.ZERO), //addition with sum=0
                Arguments.of(a, b, Monomial.constructMonomial(8, 3)),
                Arguments.of(a, j, Monomial.constructMonomial(-2.8, 3)),
                Arguments.of(a, e, a), // zero operand with nonzero exponent
                Arguments.of(a, Monomial.ZERO, a), // zero operand with zero exponent
                Arguments.of(h, i, Monomial.constructMonomial(0.2, 3)),
                Arguments.of(h, j, Monomial.constructMonomial(-3.3, 3)),
                Arguments.of(d, k, Monomial.constructMonomial(13.3, 2))));
    }

    private static List<Arguments> provideSubtractInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, a, Monomial.ZERO), //subtraction with result=0
                Arguments.of(a, b, Monomial.constructMonomial(-2, 3)),
                Arguments.of(a, j, Monomial.constructMonomial(8.8, 3)),
                Arguments.of(a, e, a), // zero operand with nonzero exponent
                Arguments.of(a, Monomial.ZERO, a), // zero operand with zero exponent
                Arguments.of(h, i, Monomial.constructMonomial(4.8, 3)),
                Arguments.of(h, j, Monomial.constructMonomial(8.3, 3)),
                Arguments.of(d, k, Monomial.constructMonomial(-9.3, 2))));
    }

    private static List<Arguments> provideMultiplyInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, a, Monomial.constructMonomial(9, 6)),
                Arguments.of(a, d, Monomial.constructMonomial(6, 5)),
                Arguments.of(a, j, Monomial.constructMonomial(-17.4, 6)),
                Arguments.of(a, e, Monomial.ZERO), // zero operand with nonzero exponent
                Arguments.of(a, Monomial.ZERO, Monomial.ZERO), // zero operand with zero exponent
                Arguments.of(h, i, Monomial.constructMonomial(-5.75, 6)),
                Arguments.of(h, j, Monomial.constructMonomial(-14.5, 6)),
                Arguments.of(d, k, Monomial.constructMonomial(22.6, 4)),
                Arguments.of(d, f, Monomial.constructMonomial(6, 2)),
                Arguments.of(d, ONE, d),
                Arguments.of(e, Monomial.ZERO, Monomial.ZERO))); //both operands are 0
    }

    private static List<Arguments> provideDivideInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, a, new Pair<>(ONE, Monomial.ZERO)),
                Arguments.of(a, d, new Pair<>(Monomial.constructMonomial(1.5, 1), Monomial.ZERO)),
                Arguments.of(a, j, new Pair<>(Monomial.constructMonomial(-0.5172413793103, 0),
                        Monomial.ZERO)),
                Arguments.of(l, a, new Pair<>(Monomial.constructMonomial(4, 2), Monomial.ZERO)),
                Arguments.of(a, l, new Pair<>(Monomial.ZERO, a)), // zero quotient, dividend
                // becomes remainder
                Arguments.of(c, d, new Pair<>(Monomial.constructMonomial(-1.5, 1), Monomial.ZERO)),
                Arguments.of(d, ONE, new Pair<>(d, Monomial.ZERO))));
    }

    private static List<Arguments> provideDifferentiateInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, Monomial.constructMonomial(9, 2)),
                Arguments.of(c, Monomial.constructMonomial(-9, 2)),
                Arguments.of(e, Monomial.ZERO),
                Arguments.of(n, Monomial.ZERO),
                Arguments.of(h, Monomial.constructMonomial(7.5, 2)),
                Arguments.of(k, Monomial.constructMonomial(22.6, 1))));
    }

    private static List<Arguments> provideIntegrateInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, Monomial.constructMonomial(0.75, 4)),
                Arguments.of(c, Monomial.constructMonomial(-0.75, 4)),
                Arguments.of(e, Monomial.ZERO), //coefficient 0
                Arguments.of(n, Monomial.constructMonomial(12, 1)), //exponent 0
                Arguments.of(h, Monomial.constructMonomial(0.625, 4)),
                Arguments.of(k, Monomial.constructMonomial(3.76666666666, 3))));
    }

    private static List<Arguments> provideOppositeInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, Monomial.constructMonomial(-3, 3)),
                Arguments.of(c, Monomial.constructMonomial(3, 3)),
                Arguments.of(e, Monomial.ZERO), //coefficient 0
                Arguments.of(k, Monomial.constructMonomial(-11.3, 2))));
    }
}