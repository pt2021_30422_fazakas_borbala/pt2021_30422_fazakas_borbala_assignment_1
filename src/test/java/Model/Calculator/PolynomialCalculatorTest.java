package Model.Calculator;

import Model.Data.Monomial;
import Model.Data.Polynomial;
import javafx.util.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class PolynomialCalculatorTest {

    private static Polynomial a, b, c, d, min_a, a_plus_b, a_minus_b, a_times_b, ONE, MIN_ONE,
            a_div_c_quot, a_div_c_rem, a_diff, b_diff, d_int, a_int, b_int;
    private static PolynomialCalculator polynomialCalculator;

    @BeforeAll
    static void setUp() {
        polynomialCalculator = PolynomialCalculator.getInstance();
        a = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(15, 10))
                .addMonomial(Monomial.constructMonomial(8, 8))
                .addMonomial(Monomial.constructMonomial(8, 6))
                .addMonomial(Monomial.constructMonomial(-6, 2))
                .addMonomial(Monomial.constructMonomial(1, 0))
                .build();
        min_a = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(-15, 10))
                .addMonomial(Monomial.constructMonomial(-8, 8))
                .addMonomial(Monomial.constructMonomial(-8, 6))
                .addMonomial(Monomial.constructMonomial(6, 2))
                .addMonomial(Monomial.constructMonomial(-1, 0))
                .build();
        b = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(5, 10))
                .addMonomial(Monomial.constructMonomial(8, 8))
                .addMonomial(Monomial.constructMonomial(2, 7))
                .addMonomial(Monomial.constructMonomial(6, 5))
                .addMonomial(Monomial.constructMonomial(-3, 4))
                .addMonomial(Monomial.constructMonomial(-1, 0))
                .build();
        c = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(3, 5))
                .addMonomial(Monomial.constructMonomial(2, 3))
                .build();
        d = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(3, 0))
                .build();
        a_plus_b = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(20, 10))
                .addMonomial(Monomial.constructMonomial(16, 8))
                .addMonomial(Monomial.constructMonomial(2, 7))
                .addMonomial(Monomial.constructMonomial(8, 6))
                .addMonomial(Monomial.constructMonomial(6, 5))
                .addMonomial(Monomial.constructMonomial(-3, 4))
                .addMonomial(Monomial.constructMonomial(-6, 2))
                .build();
        a_minus_b = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(10, 10))
                .addMonomial(Monomial.constructMonomial(-2, 7))
                .addMonomial(Monomial.constructMonomial(8, 6))
                .addMonomial(Monomial.constructMonomial(-6, 5))
                .addMonomial(Monomial.constructMonomial(3, 4))
                .addMonomial(Monomial.constructMonomial(-6, 2))
                .addMonomial(Monomial.constructMonomial(2, 0))
                .build();
        a_times_b = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(75, 20))
                .addMonomial(Monomial.constructMonomial(160, 18))
                .addMonomial(Monomial.constructMonomial(30, 17))
                .addMonomial(Monomial.constructMonomial(104, 16))
                .addMonomial(Monomial.constructMonomial(106, 15))
                .addMonomial(Monomial.constructMonomial(19, 14))
                .addMonomial(Monomial.constructMonomial(64, 13))
                .addMonomial(Monomial.constructMonomial(-54, 12))
                .addMonomial(Monomial.constructMonomial(48, 11))
                .addMonomial(Monomial.constructMonomial(-82, 10))
                .addMonomial(Monomial.constructMonomial(-12, 9))
                .addMonomial(Monomial.constructMonomial(-34, 7))
                .addMonomial(Monomial.constructMonomial(10, 6))
                .addMonomial(Monomial.constructMonomial(6, 5))
                .addMonomial(Monomial.constructMonomial(-3, 4))
                .addMonomial(Monomial.constructMonomial(6, 2))
                .addMonomial(Monomial.constructMonomial(-1, 0))
                .build();
        ONE = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(1, 0))
                .build();
        MIN_ONE = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(-1, 0))
                .build();
        a_div_c_quot = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(5, 5))
                .addMonomial(Monomial.constructMonomial(-0.6666667, 3))
                .addMonomial(Monomial.constructMonomial(3.1111111, 1))
                .build();
        a_div_c_rem = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(-6.2222222, 4))
                .addMonomial(Monomial.constructMonomial(-6, 2))
                .addMonomial(Monomial.constructMonomial(1, 0))
                .build();
        a_diff = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(150, 9))
                .addMonomial(Monomial.constructMonomial(64, 7))
                .addMonomial(Monomial.constructMonomial(48, 5))
                .addMonomial(Monomial.constructMonomial(-12, 1))
                .build();
        b_diff = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(50, 9))
                .addMonomial(Monomial.constructMonomial(64, 7))
                .addMonomial(Monomial.constructMonomial(14, 6))
                .addMonomial(Monomial.constructMonomial(30, 4))
                .addMonomial(Monomial.constructMonomial(-12, 3))
                .build();
        a_int = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(1.363636363, 11))
                .addMonomial(Monomial.constructMonomial(0.888888888, 9))
                .addMonomial(Monomial.constructMonomial(1.142857142, 7))
                .addMonomial(Monomial.constructMonomial(-2, 3))
                .addMonomial(Monomial.constructMonomial(1, 1))
                .build();
        b_int = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(0.45454545, 11))
                .addMonomial(Monomial.constructMonomial(0.888888888, 9))
                .addMonomial(Monomial.constructMonomial(0.25, 8))
                .addMonomial(Monomial.constructMonomial(1, 6))
                .addMonomial(Monomial.constructMonomial(-0.6, 5))
                .addMonomial(Monomial.constructMonomial(-1, 1))
                .build();
        d_int = new Polynomial.StandardBuilder()
                .addMonomial(Monomial.constructMonomial(3, 1))
                .build();
    }

    @ParameterizedTest
    @MethodSource("provideAddInput")
    void add(Polynomial p1, Polynomial p2, Polynomial expected) {
        Polynomial actual = polynomialCalculator.add(p1, p2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed polynomial addition: %s + %s", p1, p2));
    }

    @ParameterizedTest
    @MethodSource("provideSubtractInput")
    void subtract(Polynomial p1, Polynomial p2, Polynomial expected) {
        Polynomial actual = polynomialCalculator.subtract(p1, p2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed polynomial addition: %s - %s", p1, p2));
    }

    @ParameterizedTest
    @MethodSource("provideMultiplyInput")
    void multiply(Polynomial p1, Polynomial p2, Polynomial expected) {
        Polynomial actual = polynomialCalculator.multiply(p1, p2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed polynomial addition: %s * %s", p1, p2));
    }

    @ParameterizedTest
    @MethodSource("provideDivideInput")
    void divide(Polynomial p1, Polynomial p2, Pair<Polynomial, Polynomial> expected) {
        Pair<Polynomial, Polynomial> actual = polynomialCalculator.divide(p1, p2);
        Assertions.assertEquals(expected, actual,
                String.format("Failed polynomial addition: %s / %s", p1, p2));
    }

    @Test
    void divide_zeroDivisor_expectException() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            polynomialCalculator.divide(a, Polynomial.ZERO);
        });
    }

    @ParameterizedTest
    @MethodSource("provideDifferentiateInput")
    void differentiate(Polynomial p, Polynomial expected) {
        Polynomial actual = polynomialCalculator.differentiate(p);
        Assertions.assertEquals(expected, actual,
                String.format("Failed polynomial addition: (%s)'", p));
    }

    @ParameterizedTest
    @MethodSource("provideIntegrateInput")
    void integrate(Polynomial p, Polynomial expected) {
        Polynomial actual = polynomialCalculator.integrate(p);
        Assertions.assertEquals(expected, actual,
                String.format("Failed polynomial addition: integral(%s)", p));
    }

    private static List<Arguments> provideAddInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, min_a, Polynomial.ZERO),
                Arguments.of(a, b, a_plus_b),
                Arguments.of(a, Polynomial.ZERO, a)));
    }

    private static List<Arguments> provideSubtractInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, b, a_minus_b),
                Arguments.of(a, Polynomial.ZERO, a),
                Arguments.of(Polynomial.ZERO, a, min_a)));
    }

    private static List<Arguments> provideMultiplyInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, b, a_times_b),
                Arguments.of(a, Polynomial.ZERO, Polynomial.ZERO),
                Arguments.of(a, ONE, a),
                Arguments.of(a, MIN_ONE, min_a)));
    }

    private static List<Arguments> provideDivideInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, c, new Pair<>(a_div_c_quot, a_div_c_rem)),
                Arguments.of(a, a, new Pair<>(ONE, Polynomial.ZERO)),
                Arguments.of(a, ONE, new Pair<>(a, Polynomial.ZERO)),
                Arguments.of(a, MIN_ONE, new Pair<>(min_a, Polynomial.ZERO)),
                Arguments.of(Polynomial.ZERO, a, new Pair<>(Polynomial.ZERO, Polynomial.ZERO))));
    }

    private static List<Arguments> provideDifferentiateInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, a_diff),
                Arguments.of(b, b_diff),
                Arguments.of(d, Polynomial.ZERO),
                Arguments.of(Polynomial.ZERO, Polynomial.ZERO)));
    }

    private static List<Arguments> provideIntegrateInput() {
        return new ArrayList<>(Arrays.asList(
                Arguments.of(a, a_int),
                Arguments.of(b, b_int),
                Arguments.of(d, d_int),
                Arguments.of(Polynomial.ZERO, Polynomial.ZERO)));
    }
}