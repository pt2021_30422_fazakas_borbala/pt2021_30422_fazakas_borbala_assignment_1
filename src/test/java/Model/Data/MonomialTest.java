package Model.Data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MonomialTest {

    @Test
    void constructMonomial() {
        Monomial monomial = Monomial.constructMonomial(3.5, 4);
        assertEquals(3.5, monomial.getCoeff());
        assertEquals(4, monomial.getExponent());
    }

    @Test
    void constructMonomial_zeroCoeff_expectZeroExponent() {
        Monomial monomial = Monomial.constructMonomial(0, 3);
        assertEquals(0, monomial.getExponent());
    }

    @Test
    void constructMonomial_negativeExponent_expectException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Monomial.constructMonomial(1, -1);
        });
    }

    @Test
    void testEquals_integerCoeff() {
        Monomial monomial1 = Monomial.constructMonomial(3, 4);
        Monomial monomial2 = Monomial.constructMonomial(3, 4);
        assertEquals(monomial1, monomial2);
    }

    @Test
    void testEquals_doubleCoeff() {
        Monomial monomial1 = Monomial.constructMonomial(3.3456789, 4);
        Monomial monomial2 = Monomial.constructMonomial(3.3456788, 4);
        assertEquals(monomial1, monomial2);
    }

    @Test
    void testEquals_zeroCoeff_expectZero() {
        Monomial zero = Monomial.constructMonomial(0, 4);
        assertEquals(Monomial.ZERO, zero);
    }
}