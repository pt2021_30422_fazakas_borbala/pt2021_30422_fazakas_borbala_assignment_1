package Model.Data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PolynomialTest {

    @Test
    void testStandardBuild_distinctExponents() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 15),
                Monomial.constructMonomial(7, 11),
                Monomial.constructMonomial(0, 11), //not considered
                Monomial.constructMonomial(8, 3),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder = new Polynomial.StandardBuilder();
        for (Monomial monomial : addedMons) {
            builder = builder.addMonomial(monomial);
        }

        List<Monomial> expectedMons = List.of(
                Monomial.constructMonomial(3, 15),
                Monomial.constructMonomial(7, 11),
                Monomial.constructMonomial(8, 3),
                Monomial.constructMonomial(1, 0)
        );

        Polynomial polynomial = builder.build();

        assertEquals(expectedMons, polynomial.getTerms());
    }

    @Test
    void testStandardBuild_nonDistinctExponents() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 15),
                Monomial.constructMonomial(5, 12),
                Monomial.constructMonomial(-5, 12),
                Monomial.constructMonomial(7, 11),
                Monomial.constructMonomial(8, 11),
                Monomial.constructMonomial(0, 11),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder = new Polynomial.StandardBuilder();
        for (Monomial monomial : addedMons) {
            builder = builder.addMonomial(monomial);
        }

        List<Monomial> expectedMons = List.of(
                Monomial.constructMonomial(3, 15),
                Monomial.constructMonomial(15, 11),
                Monomial.constructMonomial(1, 0)
        );

        Polynomial polynomial = builder.build();

        assertEquals(expectedMons, polynomial.getTerms());
    }

    @Test
    void testStandardBuild_nonDecreasingExponents_expectException() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 15),
                Monomial.constructMonomial(7, 11),
                Monomial.constructMonomial(8, 20),
                Monomial.constructMonomial(1, 0)
        );

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            PolynomialBuilder builder = new Polynomial.StandardBuilder();
            for (Monomial monomial : addedMons) {
                builder = builder.addMonomial(monomial);
            }
        });
    }

    @Test
    void testRandomBuild_distinctExponents() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 11),
                Monomial.constructMonomial(7, 8),
                Monomial.constructMonomial(0, 9), //not considered
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 17),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons) {
            builder = builder.addMonomial(monomial);
        }

        Polynomial polynomial = builder.build();

        List<Monomial> expectedMons = new ArrayList<>(List.of(
                Monomial.constructMonomial(3, 11),
                Monomial.constructMonomial(7, 8),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 17),
                Monomial.constructMonomial(1, 0)
        ));
        Collections.sort(expectedMons, new Monomial.ExponentComparator().reversed());

        assertEquals(expectedMons, polynomial.getTerms());
    }

    @Test
    void testRandomBuild_nonDistinctExponents() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 0),
                Monomial.constructMonomial(5, 12),
                Monomial.constructMonomial(7, 0),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(-5, 12),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons) {
            builder = builder.addMonomial(monomial);
        }

        Polynomial polynomial = builder.build();

        List<Monomial> expectedMons = List.of(
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(16, 15),
                Monomial.constructMonomial(11, 0)
        );

        assertEquals(expectedMons, polynomial.getTerms());
    }

    @Test
    void testEquals_equalPolynomials() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 0),
                Monomial.constructMonomial(5, 12),
                Monomial.constructMonomial(7, 0),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(-5, 12),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder1 = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons) {
            builder1 = builder1.addMonomial(monomial);
        }
        Polynomial polynomial1 = builder1.build();

        PolynomialBuilder builder2 = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons) {
            builder2 = builder2.addMonomial(monomial);
        }
        Polynomial polynomial2 = builder2.build();

        assertEquals(polynomial1, polynomial2);
    }

    @Test
    void testEquals_NonEqualPolynomials() {
        List<Monomial> addedMons1 = List.of(
                Monomial.constructMonomial(3, 0),
                Monomial.constructMonomial(5, 12),
                Monomial.constructMonomial(7, 0),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(-5, 12),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder1 = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons1) {
            builder1 = builder1.addMonomial(monomial);
        }
        Polynomial polynomial1 = builder1.build();

        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 0),
                Monomial.constructMonomial(5, 12),
                Monomial.constructMonomial(7, 0),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(-6, 12),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder2 = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons) {
            builder2 = builder2.addMonomial(monomial);
        }
        Polynomial polynomial2 = builder2.build();

        assertNotEquals(polynomial1, polynomial2);
    }

    @Test
    void testEquals_emptyPolynomials() {
        Polynomial polynomial = new Polynomial.StandardBuilder().build();
        assertEquals(Polynomial.ZERO, polynomial);
    }

    @Test
    void getDegree_nonEmptyPolynomial() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 0),
                Monomial.constructMonomial(5, 12),
                Monomial.constructMonomial(7, 0),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(-5, 12),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons) {
            builder = builder.addMonomial(monomial);
        }
        Polynomial polynomial1 = builder.build();
        assertEquals(20, polynomial1.getDegree());
    }

    @Test
    void getDegree_emptyPolynomial() {
        Polynomial polynomial = new Polynomial.RandomBuilder().build();
        assertEquals(0, polynomial.getDegree());
    }

    @Test
    void getDominantTerm_nonEmptyPolynomial() {
        List<Monomial> addedMons = List.of(
                Monomial.constructMonomial(3, 0),
                Monomial.constructMonomial(5, 12),
                Monomial.constructMonomial(1, 20),
                Monomial.constructMonomial(7, 0),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(12, 20),
                Monomial.constructMonomial(8, 15),
                Monomial.constructMonomial(-5, 12),
                Monomial.constructMonomial(-8, 20),
                Monomial.constructMonomial(1, 0)
        );

        PolynomialBuilder builder = new Polynomial.RandomBuilder();
        for (Monomial monomial : addedMons) {
            builder = builder.addMonomial(monomial);
        }
        Polynomial polynomial = builder.build();
        assertEquals(Monomial.constructMonomial(5, 20), polynomial.getDominantTerm());
    }

    @Test
    void getDominantTerm_emptyPolynomial() {
        Polynomial polynomial = new Polynomial.RandomBuilder().build();
        assertEquals(Monomial.ZERO, polynomial.getDominantTerm());
    }
}