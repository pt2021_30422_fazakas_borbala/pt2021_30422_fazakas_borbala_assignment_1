package Model.Exceptions;

public class DifferentVariablePolynomialsException extends Exception {

    private final String polynA, polynB;
    private final char variableA, variableB;

    public DifferentVariablePolynomialsException(String polynA, String polynB, char variableA, char variableB) {
        super("The calculator doesn't support operations on polynomials with different variables");
        this.polynA = polynA;
        this.polynB = polynB;
        this.variableA = variableA;
        this.variableB = variableB;
    }

    public String getPolynA() {
        return polynA;
    }

    public String getPolynB() {
        return polynB;
    }

    public char getVariableA() {
        return variableA;
    }

    public char getVariableB() {
        return variableB;
    }
}
