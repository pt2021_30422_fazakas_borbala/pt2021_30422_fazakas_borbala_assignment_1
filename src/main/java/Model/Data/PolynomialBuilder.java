package Model.Data;

public interface PolynomialBuilder {
    PolynomialBuilder addMonomial(Monomial term);

    Polynomial build();
}
