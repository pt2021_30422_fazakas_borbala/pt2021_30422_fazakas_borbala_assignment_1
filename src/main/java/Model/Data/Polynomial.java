package Model.Data;

import Model.Calculator.MonomialCalculator;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * Represents a polynomial as known in mathematics: it consists of a list (terms) of monomials
 * with non-zero coefficients, ordered by their exponents, in strictly descending order. The
 * monomials have distinct exponents.
 * <p>
 * Polynomials are immutable, and can be constructed with two different builders, based on the
 * Builder Pattern:
 * 1) if the client can guarantee that the monomials will be added in descending order, considering
 * the exponents, the StandardFormBuilder should be used, which constructs a Polynomial
 * in linear time (and throws an exception if the above mentioned rule is not obeyed).
 * 2) otherwise, the RandomBuilder should be used, which doesn't assume any particular
 * order of the monomials added to the polynomial, so it sorts them explicitly before actually
 * constructing the Polynomial. Thus, construction is O(nlogn).
 * In both cases, the added monomials may have equal exponents, the builder will reduce these
 * monomials into one.
 */
public final class Polynomial {
    private final List<Monomial> terms;

    private Polynomial(List<Monomial> terms) {
        this.terms = terms;
    }

    public static Polynomial ZERO = new StandardBuilder().build();

    public static class StandardBuilder implements PolynomialBuilder {
        private final List<Monomial> terms;
        private final Monomial.ExponentComparator comparator;

        public StandardBuilder() {
            terms = new ArrayList<>();
            comparator = new Monomial.ExponentComparator();
        }

        @Override
        public StandardBuilder addMonomial(Monomial term) throws IllegalArgumentException {
            if (!term.equals(Monomial.ZERO)) {
                if (terms.isEmpty()) {
                    terms.add(term);
                } else {
                    int lastIndex = terms.size() - 1;
                    switch (Integer.signum(comparator.compare(Iterables.getLast(terms), term))) {
                        case 1: // term has smaller exponent than the previous one
                            terms.add(term);
                            break;
                        case 0: // term has the same exponent as the previous one --> they need to be
                            // merged into one single term
                            Monomial prev = terms.remove(lastIndex);
                            Monomial sum = MonomialCalculator.getInstance().add(prev, term);
                            if (!sum.equals(Monomial.ZERO)) {
                                terms.add(sum);
                            }
                            break;
                        case -1: // term has larger coefficient than the previous one
                            throw new IllegalArgumentException();
                    }
                }
            }
            return this;
        }

        @Override
        public Polynomial build() {
            return new Polynomial(terms);
        }
    }

    public static class RandomBuilder implements PolynomialBuilder {
        private final TreeMap<Integer, Monomial> exponToTerms;

        public RandomBuilder() {
            exponToTerms = new TreeMap<>(Collections.reverseOrder());
        }

        @Override
        public RandomBuilder addMonomial(Monomial term) throws IllegalArgumentException {
            if (!term.equals(Monomial.ZERO)) {
                if (exponToTerms.containsKey(term.getExponent())) {
                    // a monomial with the same exponent already exists, they need to be merged
                    // into one
                    Monomial otherTerm = exponToTerms.get(term.getExponent());
                    Monomial sum = MonomialCalculator.getInstance().add(otherTerm, term);
                    if (!sum.equals(Monomial.ZERO)) {
                        exponToTerms.put(term.getExponent(), sum);
                    } else {
                        exponToTerms.remove(term.getExponent());
                    }
                } else {
                    exponToTerms.put(term.getExponent(), term);
                }
            }
            return this;
        }

        @Override
        public Polynomial build() {
            return new Polynomial(new ArrayList<>(exponToTerms.values()));
        }
    }

    public List<Monomial> getTerms() {
        return Collections.unmodifiableList(terms);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Polynomial)) return false;
        Polynomial that = (Polynomial) o;
        return terms.equals(that.terms);
    }

    public int getDegree() {
        if (terms.isEmpty()) {
            return 0;
        } else {
            return terms.get(0).getExponent();
        }
    }

    public Monomial getDominantTerm() {
        if (terms.isEmpty()) {
            return Monomial.ZERO;
        } else {
            return terms.get(0);
        }
    }

    @Override
    public String toString() {
        if (terms.isEmpty()) {
            return "0";
        }
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Monomial monomial : terms) {
            if (!monomial.toString().isEmpty()) {
                if (monomial.toString().charAt(0) != '-' && !first) {
                    result.append("+");
                }
                result.append(monomial.toString());
                first = false;
            }
        }
        return result.toString();
    }
}
