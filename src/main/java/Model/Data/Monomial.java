package Model.Data;

import com.google.common.math.DoubleMath;

import java.text.DecimalFormat;
import java.util.Comparator;

/**
 * Represents a monomial, i.e. a one-term polynomial, with a floating-point coefficient and o
 * positive (>=0) integer exponent, representing coeff * x^expon;
 * Monomials are immutable.
 */
public final class Monomial {
    private final double coeff;
    private final int expon;

    public static final double EPSILON = 0.000001d; //used for double comparison

    public static final Monomial ZERO = new Monomial(0, 0);

    public static final char DEFAULT_VARIABLE_CHARACTER = 'x';

    private Monomial(double coeff, int expon) {
        this.coeff = coeff;
        this.expon = expon;
    }

    public static Monomial constructMonomial(double coeff, int expon) {
        if (coeff == 0) {
            expon = 0;
        }
        if (expon < 0) {
            throw new IllegalArgumentException("Exponent must be >= 0");
        }
        return new Monomial(coeff, expon);
    }

    public double getCoeff() {
        return coeff;
    }

    public int getExponent() {
        return expon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Monomial monomial = (Monomial) o;
        return (DoubleMath.fuzzyEquals(this.coeff, monomial.coeff, EPSILON)
                && this.expon == monomial.expon) ||
                (DoubleMath.fuzzyEquals(this.coeff, 0d, EPSILON) &&
                        DoubleMath.fuzzyEquals(monomial.coeff, 0d, EPSILON));
    }

    public static class ExponentComparator implements Comparator<Monomial> {

        @Override
        public int compare(Monomial o1, Monomial o2) {
            return Integer.compare(o1.expon, o2.expon);
        }
    }

    @Override
    public String toString() {
        String result = "";
        if ((!DoubleMath.fuzzyEquals(this.coeff, 1, EPSILON) &&
                !DoubleMath.fuzzyEquals(this.coeff, -1, EPSILON)) || expon == 0) {
            if (DoubleMath.fuzzyEquals(this.coeff, Math.round(this.coeff), EPSILON)) {
                result += String.valueOf(Math.round(this.coeff));
            } else {
                result += String.valueOf(new DecimalFormat("#.###").format(coeff));
            }
        }
        if (DoubleMath.fuzzyEquals(this.coeff, -1, EPSILON) && expon != 0) {
            result += "-";
        }
        if (expon > 0) {
            result += DEFAULT_VARIABLE_CHARACTER;
            if (expon > 1) {
                result += "^" + expon;
            }
        }
        return result;
    }
}
