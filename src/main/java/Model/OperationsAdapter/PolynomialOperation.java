package Model.OperationsAdapter;

import Model.Calculator.PolynomialCalculator;
import Model.Data.Polynomial;
import javafx.util.Pair;

import java.util.Objects;
import java.util.Optional;

/**
 * PolynomialOperation is an enum which defines the operations supported by the application, in
 * the format specified by the OperationStrategy interface.
 * The members of the enum are the strategies.
 */
public enum PolynomialOperation implements OperationStrategy<Polynomial> {
    ADD(false, true, "Sum", null,
            (p1, p2) ->
                    new Pair<>(PolynomialCalculator.getInstance().add(Objects.requireNonNull(p1),
                            Objects.requireNonNull(p2)),
                            Optional.empty())),
    SUBTRACT(false, true, "Difference", null,
            (p1, p2) ->
                    new Pair<>(PolynomialCalculator.getInstance().subtract(Objects.requireNonNull(p1),
                            Objects.requireNonNull(p2)),
                            Optional.empty())),
    MULTIPLY(false, true, "Product", null,
            (p1, p2) ->
                    new Pair<>(PolynomialCalculator.getInstance().multiply(Objects.requireNonNull(p1),
                            Objects.requireNonNull(p2)),
                            Optional.empty())),
    DIVIDE(false, false, "Quotient", "Remainder",
            (p1, p2) -> {
                Pair<Polynomial, Polynomial> result =
                        PolynomialCalculator.getInstance().divide(Objects.requireNonNull(p1),
                                Objects.requireNonNull(p2));
                return new Pair<>(result.getKey(), Optional.of(result.getValue()));
            }),
    DIFFERENTIATE(true, true, "Derivative", null,
            (p1, p2) ->
                    new Pair<>(PolynomialCalculator.getInstance().differentiate(Objects.requireNonNull(p1)),
                            Optional.empty())),
    INTEGRATE(true, true, "Antiderivative", null,
            (p1, p2) ->
                    new Pair<>(PolynomialCalculator.getInstance().integrate(Objects.requireNonNull(p1)),
                            Optional.empty()));

    private final boolean unaryOperator;
    private final boolean unaryResult;
    private final OperationStrategy<Polynomial> operationStrategy;
    private final String result1Name; //primary result
    private final String result2Name; //secondary result, exists only for some operations (such
    // as division)

    PolynomialOperation(boolean unaryOperator, boolean unaryResult, String result1Name,
                        String result2Name,
                        OperationStrategy<Polynomial> operationStrategy) {
        this.unaryOperator = unaryOperator;
        this.unaryResult = unaryResult;
        this.result1Name = result1Name;
        this.result2Name = result2Name;
        this.operationStrategy = operationStrategy;
    }

    public boolean isUnaryOperator() {
        return unaryOperator;
    }

    public boolean hasUnaryResult() {
        return unaryResult;
    }

    public String getResult1Name() {
        return result1Name;
    }

    public String getResult2Name() {
        if (hasUnaryResult()) {
            throw new IllegalArgumentException("An operation with a unary result doesn't have a " +
                    "name for a second result");
        } else {
            return result2Name;
        }
    }

    @Override
    public Pair<Polynomial, Optional<Polynomial>> compute(Polynomial a, Polynomial b) {
        return operationStrategy.compute(a, b);
    }
}
