package Model.OperationsAdapter;

import javafx.util.Pair;

import java.util.Optional;

/**
 * OperationStrategy is a generic interface which defines a format for operations.
 * It behaves as an adapter, in the sense that it allows all operations to be handled ad binary
 * operations with two results.
 * On the other hand, it supports the strategy pattern, in that it allows for choosing the
 * operation to be performed but handling all operations in a uniform way.
 */
public interface OperationStrategy<T> {
    Pair<T, Optional<T>> compute(T a, T b);
}
