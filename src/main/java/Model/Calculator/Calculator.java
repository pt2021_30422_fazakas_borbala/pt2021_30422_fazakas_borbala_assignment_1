package Model.Calculator;

import javafx.util.Pair;

/**
 * Generic calculator which defines the operations a calculator should support.
 */
public interface Calculator<T> {
    T add(T a, T b);

    T subtract(T a, T b);

    T multiply(T a, T b);

    Pair<T, T> divide(T a, T b);

    T differentiate(T a);

    T integrate(T a);
}
