package Model.Calculator;

import Model.Data.Monomial;
import Model.Data.Polynomial;
import Model.Data.PolynomialBuilder;
import javafx.util.Pair;

import java.util.Collection;
import java.util.Iterator;

public class PolynomialCalculator implements Calculator<Polynomial> {

    private static PolynomialCalculator instance;
    private final MonomialCalculator monomialCalculator;

    private PolynomialCalculator() {
        monomialCalculator = MonomialCalculator.getInstance();
    }

    public static PolynomialCalculator getInstance() {
        if (instance == null) {
            instance = new PolynomialCalculator();
        }
        return instance;
    }

    @Override
    public Polynomial add(Polynomial a, Polynomial b) {
        PolynomialBuilder resultBuilder = new Polynomial.StandardBuilder();

        Iterator<Monomial> aIt = a.getTerms().iterator();
        Iterator<Monomial> bIt = b.getTerms().iterator();

        Monomial aMon = aIt.hasNext() ? aIt.next() : null;
        Monomial bMon = bIt.hasNext() ? bIt.next() : null;

        Monomial.ExponentComparator comparator = new Monomial.ExponentComparator();
        while (aMon != null || bMon != null) {
            if (bMon == null || (aMon != null && comparator.compare(aMon, bMon) >= 0)) {
                // next monomial in polynomial a is next in descending order
                resultBuilder = resultBuilder.addMonomial(aMon);
                aMon = aIt.hasNext() ? aIt.next() : null;
            } else {
                // next monomial in polynomial b is next in descending order
                resultBuilder = resultBuilder.addMonomial(bMon);
                bMon = bIt.hasNext() ? bIt.next() : null;
            }
        }
        return resultBuilder.build();
    }

    @Override
    public Polynomial subtract(Polynomial a, Polynomial b) {
        PolynomialBuilder resultBuilder = new Polynomial.StandardBuilder();

        Iterator<Monomial> aIt = a.getTerms().iterator();
        Iterator<Monomial> bIt = b.getTerms().iterator();

        Monomial aMon = aIt.hasNext() ? aIt.next() : null;
        Monomial bMon = bIt.hasNext() ? bIt.next() : null;

        Monomial.ExponentComparator comparator = new Monomial.ExponentComparator();
        while (aMon != null || bMon != null) {
            if (bMon == null || (aMon != null && comparator.compare(aMon, bMon) > 0)) {
                resultBuilder = resultBuilder.addMonomial(aMon);
                aMon = aIt.hasNext() ? aIt.next() : null;
            } else if (aMon == null || comparator.compare(aMon, bMon) < 0) {
                resultBuilder = resultBuilder.addMonomial(monomialCalculator.opposite(bMon));
                bMon = bIt.hasNext() ? bIt.next() : null;
            } else {
                resultBuilder = resultBuilder.addMonomial(monomialCalculator.subtract(aMon, bMon));
                aMon = aIt.hasNext() ? aIt.next() : null;
                bMon = bIt.hasNext() ? bIt.next() : null;
            }
        }
        return resultBuilder.build();
    }

    @Override
    public Polynomial multiply(Polynomial a, Polynomial b) {
        PolynomialBuilder resultBuilder = new Polynomial.RandomBuilder();
        Collection<Monomial> aTerms = a.getTerms();
        Collection<Monomial> bTerms = b.getTerms();
        for (Monomial monA : aTerms) {
            for (Monomial monB : bTerms) {
                resultBuilder = resultBuilder.addMonomial(monomialCalculator.multiply(monA, monB));
            }
        }
        return resultBuilder.build();
    }

    @Override
    public Pair<Polynomial, Polynomial> divide(Polynomial a, Polynomial b) {
        if (b.equals(Polynomial.ZERO)) {
            throw new ArithmeticException("Division by 0");
        }
        Polynomial currA = a;
        PolynomialBuilder resultBuilder = new Polynomial.StandardBuilder();
        while (!currA.equals(Polynomial.ZERO) && currA.getDegree() >= b.getDegree()) {
            Pair<Monomial, Monomial> quotRemain = monomialCalculator.divide(
                    currA.getDominantTerm(),
                    b.getDominantTerm());
            assert (quotRemain.getValue().equals(Monomial.ZERO));
            Monomial nextTerm = quotRemain.getKey();
            resultBuilder = resultBuilder.addMonomial(nextTerm);
            currA = subtract(currA,
                    multiply(b, new Polynomial.StandardBuilder().addMonomial(nextTerm).build()));
        }
        return new Pair<>(resultBuilder.build(), currA);
    }

    @Override
    public Polynomial differentiate(Polynomial a) {
        PolynomialBuilder resultBuilder = new Polynomial.RandomBuilder();
        Collection<Monomial> aTerms = a.getTerms();
        for (Monomial monA : aTerms) {
            resultBuilder = resultBuilder.addMonomial(monomialCalculator.differentiate(monA));
        }
        return resultBuilder.build();
    }

    @Override
    public Polynomial integrate(Polynomial a) {
        PolynomialBuilder resultBuilder = new Polynomial.RandomBuilder();
        Collection<Monomial> aTerms = a.getTerms();
        for (Monomial monA : aTerms) {
            resultBuilder = resultBuilder.addMonomial(monomialCalculator.integrate(monA));
        }
        return resultBuilder.build();
    }
}
