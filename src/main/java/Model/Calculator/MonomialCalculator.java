package Model.Calculator;

import Model.Data.Monomial;
import javafx.util.Pair;

public class MonomialCalculator implements Calculator<Monomial> {

    private static MonomialCalculator instance;

    private MonomialCalculator() {
    }

    public static MonomialCalculator getInstance() {
        if (instance == null) {
            instance = new MonomialCalculator();
        }
        return instance;
    }

    @Override
    public Monomial add(Monomial a, Monomial b) {
        if (!a.equals(Monomial.ZERO) && !b.equals(Monomial.ZERO) && a.getExponent() != b.getExponent()) {
            throw new IllegalArgumentException("The addition of non-Monomial.ZERO monomials with different" +
                    " exponents doesn't result in a monomial.");
        }
        int resultExponent = a.equals(Monomial.ZERO) ? b.getExponent() : a.getExponent();
        return Monomial.constructMonomial(a.getCoeff() + b.getCoeff(), resultExponent);
    }

    @Override
    public Monomial subtract(Monomial a, Monomial b) {
        if (!b.equals(Monomial.ZERO) && !a.equals(Monomial.ZERO) && b.getExponent() != a.getExponent()) {
            throw new IllegalArgumentException("The subtraction of non-Monomial.ZERO monomials with " +
                    "different degrees doesn't result in a monomial.");
        }
        int resultExponent = a.equals(Monomial.ZERO) ? b.getExponent() : a.getExponent();
        return Monomial.constructMonomial(a.getCoeff() - b.getCoeff(), resultExponent);
    }

    @Override
    public Monomial multiply(Monomial a, Monomial b) {
        return Monomial.constructMonomial(a.getCoeff() * b.getCoeff(), a.getExponent() + b.getExponent());
    }

    @Override
    public Pair<Monomial, Monomial> divide(Monomial a, Monomial b) {
        if (b.equals(Monomial.ZERO)) {
            throw new ArithmeticException("Division by zero");
        }
        if (a.getExponent() >= b.getExponent()) {
            return new Pair<>(Monomial.constructMonomial(a.getCoeff() / b.getCoeff(),
                    a.getExponent() - b.getExponent()), Monomial.ZERO);
        } else {
            return new Pair<>(Monomial.ZERO, a);
        }
    }

    @Override
    public Monomial differentiate(Monomial a) {
        return Monomial.constructMonomial(a.getCoeff() * a.getExponent(), a.getExponent() - 1);
    }

    @Override
    public Monomial integrate(Monomial a) {
        return Monomial.constructMonomial(a.getCoeff() / (a.getExponent() + 1), a.getExponent() + 1);
    }

    public Monomial opposite(Monomial a) {
        return Monomial.constructMonomial(-a.getCoeff(), a.getExponent());
    }
}
