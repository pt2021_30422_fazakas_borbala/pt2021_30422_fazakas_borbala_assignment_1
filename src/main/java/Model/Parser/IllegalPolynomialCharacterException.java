package Model.Parser;

public class IllegalPolynomialCharacterException extends IllegalPolynomialStringException {

    private final int illegalCharacterPosition;

    IllegalPolynomialCharacterException(String polynomialString, int illegalCharacterPosition) {
        super(polynomialString);
        this.illegalCharacterPosition = illegalCharacterPosition;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + ", because " + getPolynomialString().charAt(illegalCharacterPosition) +
                " is illegal in a polynomial expression";
    }

    public int getIllegalCharacterPosition() {
        return illegalCharacterPosition;
    }

    public char getIllegalCharacter() {
        return getPolynomialString().charAt(illegalCharacterPosition);
    }
}
