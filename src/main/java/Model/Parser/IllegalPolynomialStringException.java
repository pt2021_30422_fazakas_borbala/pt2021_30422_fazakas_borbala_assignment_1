package Model.Parser;

public abstract class IllegalPolynomialStringException extends Exception {
    private final String polynomialString;

    IllegalPolynomialStringException(String polynomialString) {
        super(polynomialString + " does not represent a polynomial");
        this.polynomialString = polynomialString;
    }

    public String getPolynomialString() {
        return polynomialString;
    }
}
