package Model.Parser;

public class EmptyPolynomialStringException extends IllegalPolynomialStringException {
    EmptyPolynomialStringException(String polynomialString) {
        super(polynomialString);
    }
}
