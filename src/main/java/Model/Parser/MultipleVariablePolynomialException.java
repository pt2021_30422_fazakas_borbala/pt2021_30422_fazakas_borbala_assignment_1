package Model.Parser;

import java.util.Collections;
import java.util.Set;

public class MultipleVariablePolynomialException extends IllegalPolynomialStringException {
    private final Set<Character> variables;

    MultipleVariablePolynomialException(String polynomial, Set<Character> variables) {
        super(polynomial);
        this.variables = variables;
    }

    public Set<Character> getVariables() {
        return Collections.unmodifiableSet(variables);
    }

    @Override
    public String getMessage() {
        return super.getMessage() + ", multiple variables found: " + variables;
    }
}
