package Model.Parser;

import Model.Data.Monomial;
import Model.Data.Polynomial;
import Model.Data.PolynomialBuilder;
import Model.Exceptions.DifferentVariablePolynomialsException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolynomialParser {
    private static final String MONOMIAL_PATTERN_FORMAT_REGEX =
            "\\G(?<coeff>(?<coeffSign>[+-])(\\s*)(([1-9](\\d*))|0)?)(\\s*)(\\*?)(\\s*)" +
                    "(?<variable>%c(\\s*)(\\^(\\s*)(?<power>([1-9](\\d*))|0))?)?(\\s*)";

    private static final String ILLEGAL_CHARACTERS_REGEX = "[^a-zA-Z0-9\\^+\\-*\\s)]";

    public Polynomial parse(String s) throws MultipleVariablePolynomialException, IllegalPolynomialStringPartException, IllegalPolynomialCharacterException, EmptyPolynomialStringException {
        if (s.isEmpty()) {
            throw new EmptyPolynomialStringException(s);
        }
        guaranteeNoIllegalCharacters(s); // check if there are any illegal characters
        char variableCharacter = getUniqueVariableCharacter(s).orElse(Monomial.DEFAULT_VARIABLE_CHARACTER);
        // letter that represents the variable, make sure there's only one variable in the string
        String polynomialString = addLeadingSignToString(s); //ensure each monomial starts with +/-
        Matcher matcher = getPattern(variableCharacter).matcher(polynomialString);
        // preopare the matcher based on the character that represents the variable
        int lastMatchPos = -1; //used to ensure that the monomials that were found cover the
        // entire expression
        PolynomialBuilder polynomialBuilder = new Polynomial.RandomBuilder();
        // find the monomials and build the polynomial out of them
        while (matcher.find()) {
            if (matcher.start() == matcher.end() - 1) { // illegal empty monomial found
                throw new IllegalPolynomialStringPartException(s, getOriginalStringIndex(s,
                        polynomialString, matcher.start()));
            }
            // construct monomial
            int defaultCoeff = matcher.group("coeffSign").equals("-") ? -1 : 1;
            int coeff = getNumber(matcher.group("coeff"), defaultCoeff);
            int defaultExponent = matcher.group("variable") == null ? 0 : 1;
            int exponent = getNumber(matcher.group("power"), defaultExponent);
            polynomialBuilder.addMonomial(Monomial.constructMonomial(coeff, exponent));
            lastMatchPos = matcher.end();
        }
        if (lastMatchPos != polynomialString.length()) {
            // monomials don't cover the entire expression --> illegal expression
            throw new IllegalPolynomialStringPartException(polynomialString, lastMatchPos);
        } else {
            return polynomialBuilder.build();
        }
    }

    public char getOperandsSingleVariableCharacter(String polynA, String polynB) throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException {
        Optional<Character> variableA = getUniqueVariableCharacter(polynA);
        Optional<Character> variableB = getUniqueVariableCharacter(polynB);
        if (variableA.isPresent() && variableB.isPresent() && variableA.get() != variableB.get()) {
            throw new DifferentVariablePolynomialsException(polynA, polynB, variableA.get(),
                    variableB.get());
        }
        return variableA.orElse(variableB.orElse(Monomial.DEFAULT_VARIABLE_CHARACTER));
    }

    private Optional<Character> getUniqueVariableCharacter(String s) throws MultipleVariablePolynomialException {
        Set<Character> variables = getVariableCharacters(s);
        if (variables.size() == 0) {
            return Optional.empty();
        } else if (variables.size() == 1) { //valid
            return Optional.of(new ArrayList<>(variables).get(0));
        } else { //multiple variables found --> invalid
            throw new MultipleVariablePolynomialException(s, variables);
        }
    }


    // note: any letter(alphabetic character) is considered a variable
    private Set<Character> getVariableCharacters(String s) {
        Pattern p = Pattern.compile("\\p{Alpha}");
        Matcher m = p.matcher(s);
        Set<Character> variables = new HashSet<>();
        while (m.find()) {
            char variable = s.charAt(m.start());
            variables.add(variable);
        }
        return variables;
    }

    private void guaranteeNoIllegalCharacters(String s) throws IllegalPolynomialCharacterException {
        Pattern pattern = Pattern.compile(ILLEGAL_CHARACTERS_REGEX);
        Matcher matcher = pattern.matcher(s);
        if (matcher.find()) { // illegal character found
            throw new IllegalPolynomialCharacterException(s, matcher.start());
        }
    }

    private int getNumber(String expr, int defaultValue) {
        int nr = defaultValue;
        if (expr != null) {
            expr = expr.trim();
            if (expr.length() > 1 || (expr.charAt(0) != '-' && expr.charAt(0) != '+')) {
                nr = Integer.parseInt(expr.replaceAll("\\s*", ""));
            }
        }
        return nr;
    }

    private Pattern getPattern(char variableCharacter) {
        String monomialPatternString = String.format(MONOMIAL_PATTERN_FORMAT_REGEX,
                variableCharacter);
        return Pattern.compile(monomialPatternString);
    }

    private String addLeadingSignToString(String s) {
        if (s.isEmpty() || s.charAt(0) != '-' && s.charAt(0) != '+') {
            // no leading sign present, + assumed
            return '+' + s;
        } else {
            // leading sign present, no addition required
            return s;
        }
    }

    private int getOriginalStringIndex(String original, String newString, int newPos) {
        if (!original.equals(addLeadingSignToString(original))) {
            return newPos - 1;
        } else {
            return newPos;
        }
    }
}
