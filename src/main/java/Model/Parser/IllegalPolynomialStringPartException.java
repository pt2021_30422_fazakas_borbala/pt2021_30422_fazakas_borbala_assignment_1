package Model.Parser;

public class IllegalPolynomialStringPartException extends IllegalPolynomialStringException {

    private final int wrongPartStartIndex;

    IllegalPolynomialStringPartException(String polynomialString, int wrongPartStartIndex) {
        super(polynomialString);
        this.wrongPartStartIndex = wrongPartStartIndex;
    }

    public String getWrongSubstring() {
        if (wrongPartStartIndex >= 0) {
            return getPolynomialString().substring(wrongPartStartIndex);
        } else {
            return "";
        }
    }
}
