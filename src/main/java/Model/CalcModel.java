package Model;

import Model.Data.Monomial;
import Model.Data.Polynomial;
import Model.Exceptions.DifferentVariablePolynomialsException;
import Model.OperationsAdapter.PolynomialOperation;
import Model.Parser.*;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.util.Pair;

import java.util.Objects;
import java.util.Optional;

/**
 * CalcModel holds the state of the application and is the interface through which the controller
 * accesses the model.
 */
public class CalcModel {
    private final ObjectProperty<Pair<String, String>> result =
            new SimpleObjectProperty<>();
    private final ObjectProperty<PolynomialOperation> operation =
            new SimpleObjectProperty<>();

    private static final PolynomialParser parser = new PolynomialParser();

    public static final String INVALID_RESULT = "-";

    public void setOperation(PolynomialOperation operation) {
        this.operation.set(Objects.requireNonNull(operation));
    }

    public PolynomialOperation getOperation() {
        return operation.getValue();
    }

    public void compute(String polynA, String polynB) throws MultipleVariablePolynomialException,
            DifferentVariablePolynomialsException, IllegalPolynomialStringPartException,
            IllegalPolynomialCharacterException, EmptyPolynomialStringException {
        boolean success = false;
        try {
            Pair<Polynomial, Polynomial> operands = getOperands(polynA, polynB);
            Pair<Polynomial, Optional<Polynomial>> newResult =
                    operation.get().compute(operands.getKey(), operands.getValue());
            char variableCharacter = parser.getOperandsSingleVariableCharacter(polynA, polynB);
            result.set(new Pair<>(getPolynomialString(newResult.getKey(), variableCharacter),
                    getPolynomialString(newResult.getValue(), variableCharacter)));
            success = true;
        } finally {
            if (!success) {
                result.set(new Pair<>(INVALID_RESULT, INVALID_RESULT)); // invalid result
            }
        }
    }

    public String getResult1String() {
        if (result.getValue() == null) {
            throw new IllegalStateException("Result not available yet");
        }
        return result.getValue().getKey();
    }

    public String getResult2String() {
        if (result.getValue() == null) {
            throw new IllegalStateException("Result not available yet");
        }
        return result.getValue().getValue();
    }

    public ObjectProperty<Pair<String, String>> getObservableResult() {
        return result;
    }

    public ObjectProperty<PolynomialOperation> getObservableOperation() {
        return operation;
    }

    private String getPolynomialString(Polynomial p, char variableCharacter) {
        return p.toString().replace(Monomial.DEFAULT_VARIABLE_CHARACTER, variableCharacter);
    }

    private String getPolynomialString(Optional<Polynomial> p, char variableCharacter) {
        if (p.isEmpty()) {
            return "";
        }
        return p.get().toString().replace(Monomial.DEFAULT_VARIABLE_CHARACTER, variableCharacter);
    }

    private Pair<Polynomial, Polynomial> getOperands(String polynA, String polynB) throws MultipleVariablePolynomialException, DifferentVariablePolynomialsException, IllegalPolynomialStringPartException, IllegalPolynomialCharacterException, EmptyPolynomialStringException {
        Polynomial a = parser.parse(polynA);
        Polynomial b = null;
        if (!operation.getValue().isUnaryOperator()) {
            char c = parser.getOperandsSingleVariableCharacter(polynA, polynB); //assert that they
            // have the same variable
            b = new PolynomialParser().parse(polynB);
        }
        return new Pair<>(a, b);
    }
}
