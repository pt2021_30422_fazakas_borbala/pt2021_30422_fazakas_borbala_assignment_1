package Controller.Data;

import Model.OperationsAdapter.PolynomialOperation;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum OperationChoice {
    ADDITION(PolynomialOperation.ADD, "+ Add", "img/addition.png", true),
    SUBTRACTION(PolynomialOperation.SUBTRACT, "- Subtract", "img/subtraction.png", true),
    MULTIPLICATION(PolynomialOperation.MULTIPLY, "· Multiply", "img/multiplication.png", true),
    DIVISION(PolynomialOperation.DIVIDE, "÷ Divide", "img/division.png", true),
    DIFFERENTIATION(PolynomialOperation.DIFFERENTIATE, "' Differentiate", "img" +
            "/differentiation.png", false),
    INTEGRATION(PolynomialOperation.INTEGRATE, "∫ Integrate", "img/integration" +
            ".png", false);

    private final PolynomialOperation polynomialOperation;
    private final String displayString;
    private final String imgPath;
    private final boolean showOnRightSide;

    OperationChoice(PolynomialOperation polynomialOperation, String displayString, String imgPath, boolean showOnRightSide) {
        this.polynomialOperation = polynomialOperation;
        this.displayString = displayString;
        this.imgPath = imgPath;
        this.showOnRightSide = showOnRightSide;
    }

    @Override
    public String toString() {
        return displayString;
    }

    public PolynomialOperation getCorrespondingOperation() {
        return polynomialOperation;
    }

    public String getImagePath() {
        return imgPath;
    }

    public boolean isShownOnRightSide() {
        return showOnRightSide;
    }

    public static OperationChoice getCorrespondingChoice(PolynomialOperation operation) {
        return Arrays.stream(OperationChoice.values()).sequential()
                .filter(choice -> choice.polynomialOperation.equals(operation))
                .collect(Collectors.toList()).get(0);
    }
}
