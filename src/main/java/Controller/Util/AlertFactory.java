package Controller.Util;

import Model.Exceptions.DifferentVariablePolynomialsException;
import Model.Parser.EmptyPolynomialStringException;
import Model.Parser.IllegalPolynomialCharacterException;
import Model.Parser.IllegalPolynomialStringPartException;
import Model.Parser.MultipleVariablePolynomialException;
import javafx.scene.control.Alert;

public class AlertFactory {
    public static void showErrorAlert(MultipleVariablePolynomialException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Multiple variables in the polynomial");
        alert.setHeaderText("Multiple variables were found in polynomial " + e.getPolynomialString());
        alert.setContentText("Please remark that only single-variable polynomials are supported" +
                ".\nVariables found: " + e.getVariables());
        alert.show();
    }

    public static void showErrorAlert(IllegalPolynomialCharacterException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Illegal character in polynomial");
        alert.setHeaderText("An illegal character was found in polynomial " + e.getPolynomialString());
        alert.setContentText("Illegal character: " + e.getIllegalCharacter());
        alert.show();
    }

    public static void showErrorAlert(DifferentVariablePolynomialsException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Different variables in polynomials");
        alert.setHeaderText("Different variables were found in the polynomials " + e.getPolynA()
                + " and " + e.getPolynB());
        alert.setContentText("The variables are: " + e.getVariableA() + " and " + e.getVariableB());
        alert.show();
    }

    public static void showErrorAlert(IllegalPolynomialStringPartException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Different variables in polynomials");
        alert.setHeaderText("Illegal expression in polynomial " + e.getPolynomialString());
        alert.setContentText(e.getWrongSubstring() + " expected to be a monomial but does not " +
                "follow " +
                "the required format");
        alert.show();
    }

    public static void showErrorAlert(ArithmeticException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Illegal operation");
        alert.setHeaderText("An arithmetic exception occured");
        alert.setContentText(e.getMessage());
        alert.show();
    }

    public static void showErrorAlert(EmptyPolynomialStringException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Illegal operand");
        alert.setHeaderText("Empty polynomials are illegal");
        alert.setContentText("Zero must be marked by an explicit 0");
        alert.show();
    }

    public static void showErrorAlert(NumberFormatException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Illegal operand");
        alert.setHeaderText("Seems like one of the numbers in the input is invalid");
        alert.setContentText("Please make sure that the coefficients and exponents are not " +
                "greater than 2147483647");
        alert.show();
    }
}
