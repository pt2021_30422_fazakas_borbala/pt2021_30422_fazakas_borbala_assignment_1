package Controller;

import Controller.Data.OperationChoice;
import Controller.Util.AlertFactory;
import Model.CalcModel;
import Model.Exceptions.DifferentVariablePolynomialsException;
import Model.OperationsAdapter.PolynomialOperation;
import Model.Parser.EmptyPolynomialStringException;
import Model.Parser.IllegalPolynomialCharacterException;
import Model.Parser.IllegalPolynomialStringPartException;
import Model.Parser.MultipleVariablePolynomialException;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainController {

    @FXML
    public ChoiceBox<OperationChoice> operatorChoiceBox;
    @FXML
    public TextField operandATxt;
    @FXML
    public TextField operandBTxt;
    @FXML
    public TextField result1Txt;
    @FXML
    public TextField result2Txt;
    @FXML
    public Button computeBtn;
    @FXML
    public ImageView rightOperatorImg;
    @FXML
    public ImageView leftOperatorImg;
    @FXML
    public Label operandALabel;
    @FXML
    public Label operandBLabel;
    @FXML
    public Label result1Label;
    @FXML
    public Label result2Label;

    private CalcModel model;

    public void initialize() {
        computeBtn.setDefaultButton(true);
    }

    public void initModel(CalcModel model) throws FileNotFoundException {
        if (this.model != null) {
            throw new IllegalArgumentException("A Controller can have one single model");
        }
        this.model = model;
        // listen to changes in the result
        this.model.getObservableResult().addListener((obs, oldResult, newResult) -> {
            onResultChange();
        });
        // listen to changes in the operator
        this.model.getObservableOperation().addListener((obs, oldOperation, newOperation) -> {
            onOperatorChange(newOperation);
        });
        setOperationChoiceboxData();
    }

    /**
     * Invoked when the user chooses another operator in the UI.
     */
    public void onOperatorChoiceChange() {
        model.setOperation(operatorChoiceBox.getValue().getCorrespondingOperation());
    }

    /**
     * Invoked when the operator in the model successfully changes.
     */
    public void onOperatorChange(PolynomialOperation operation) {
        OperationChoice operationChoice = OperationChoice.getCorrespondingChoice(operation);
        setOperandImage(operationChoice);
        showSecondOperand(!operation.isUnaryOperator()); // set number of visible operand inputs
    }

    public void onComputeBtnPressed() {
        try {
            String operandB = model.getOperation().isUnaryOperator() ? "" : operandBTxt.getText();
            model.compute(operandATxt.getText(), operandB);
        } catch (MultipleVariablePolynomialException e) {
            AlertFactory.showErrorAlert(e);
        } catch (DifferentVariablePolynomialsException e) {
            AlertFactory.showErrorAlert(e);
        } catch (IllegalPolynomialStringPartException e) {
            AlertFactory.showErrorAlert(e);
        } catch (IllegalPolynomialCharacterException e) {
            AlertFactory.showErrorAlert(e);
        } catch (ArithmeticException e) {
            AlertFactory.showErrorAlert(e);
        } catch (EmptyPolynomialStringException e) {
            AlertFactory.showErrorAlert(e);
        } catch (NumberFormatException e) {
            AlertFactory.showErrorAlert(e);
        }
    }

    private void onResultChange() {
        result1Txt.setText(model.getResult1String());
        result2Txt.setText(model.getResult2String());
        result1Label.setText(model.getOperation().getResult1Name());
        showSecondResult(!model.getOperation().hasUnaryResult());
        if (!model.getOperation().hasUnaryResult()) {
            result2Label.setText(model.getOperation().getResult2Name());
        }
    }

    private void setOperandImage(OperationChoice operationChoice) {
        try (InputStream input =
                     getClass().getClassLoader().getResourceAsStream(operationChoice.getImagePath())) {
            assert input != null;
            Image image = new Image(input);
            rightOperatorImg.setVisible(operationChoice.isShownOnRightSide());
            leftOperatorImg.setVisible(!operationChoice.isShownOnRightSide());
            if (operationChoice.isShownOnRightSide()) {
                rightOperatorImg.setImage(image);
            } else {
                leftOperatorImg.setImage(image);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setOperationChoiceboxData() {
        operatorChoiceBox.setItems(FXCollections.observableArrayList(OperationChoice.values()));
        operatorChoiceBox.setValue(OperationChoice.ADDITION);
    }

    private void showSecondOperand(boolean show) {
        operandBTxt.setVisible(show);
        operandBLabel.setVisible(show);
    }

    private void showSecondResult(boolean show) {
        result2Label.setVisible(show);
        result2Txt.setVisible(show);
    }
}
