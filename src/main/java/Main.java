import Controller.MainController;
import Model.CalcModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader mainStageLoader = new FXMLLoader(getClass().getResource("/main_stage.fxml"));
        Parent root = mainStageLoader.load();

        CalcModel model = new CalcModel();
        MainController controller = mainStageLoader.getController();
        controller.initModel(model);

        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Polynomial Calculator");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
